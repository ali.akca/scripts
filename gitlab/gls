#!/usr/bin/env bash

BASE_DIR="/devel/workspace"

GL_TOKEN=${GL_TOKEN}
GL_API="https://${GL_URL}/api/v4"

PROJECTS=$(curl -s --header "Private-Token: ${GL_TOKEN}" ${GL_API}/projects?per_page=100);

for PROJECT in $(echo $PROJECTS | jq -c '.[] | {path_with_namespace: .path_with_namespace, ssh_url_to_repo: .ssh_url_to_repo}'); do
  DIR=$(echo $PROJECT | jq -r ".path_with_namespace")

  echo "Sync started for $DIR"

  PROJECT_PATH="$BASE_DIR/$GL_URL/$DIR"

  if !(test -d $PROJECT_PATH); then
    mkdir -p $PROJECT_PATH && echo "Creating Project dir : $PROJECT_PATH"
  fi

  GIT_PATH="$PROJECT_PATH/.git"

  if !(test -d $GIT_PATH); then
    REPO=$(echo $PROJECT | jq -r ".ssh_url_to_repo")

    echo "Creating git repo $REPO"

    ( cd $PROJECT_PATH && git init && git remote add origin $REPO && git pull origin master && git branch --set-upstream-to=origin/master master)
  fi

  echo "Pulling repository changes"

  ( cd $PROJECT_PATH && git fetch --all && git pull origin && git branch -r | grep -v '\->' | while read remote; do if ! [[ $(git rev-parse -q --verify "$remote") ]]; then git branch --track "${remote#origin/}" "$remote"; fi done )

  echo "Sync finished for $DIR"
done
