
Collection of useful scripts

## Gitlab scripts

Gitlab related scripts

### gls ( Gitlab Sync ):

This script automate clone and sync process for all your projects for initial setup or batch update
For using this script you need to define :

```bash
export GL_URL='gitlab.yourdomain.com'
export GL_TOKEN=your_api_token
```

For running directly from source :

```
bash <(curl -ks https://gitlab.altpro.com.tr/ali.akca/scripts/raw/master/gitlab/gls)
```

Adding alias to oh-my-zsh :

- Run `vi ~/.zshrc`

- Add `alias gls="bash <(curl -s https://gitlab.altpro.com.tr/ali.akca/scripts/raw/master/gitlab/gls)"` to file

- Run `source ~/.zshrc` reload file

__!!!NOTE!!!__ Please make sure you have [jq](https://stedolan.github.io/jq/) at your computer before using this script

## OSX scripts

OSX bootstrap scripts

### update :

This script make necessary updates for operating system. You need to run this script first when bootstrap you machine

For running directly from source :

```
bash <(curl -ks https://gitlab.altpro.com.tr/ali.akca/scripts/raw/master/osx/update)
```


### bootstrap :

 This script install all useful and necessary tools.

 For running directly from source :

```
bash <(curl -ks https://gitlab.altpro.com.tr/ali.akca/scripts/raw/master/osx/bootstrap)
```


### fs :

 This script creates base file system under '/devel' directory

 For running directly from source :

```
bash <(curl -ks https://gitlab.altpro.com.tr/ali.akca/scripts/raw/master/osx/fs)
```
